package com.example.unitytest;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

import com.smartlook.sdk.smartlook.Smartlook;
import com.smartlook.sdk.smartlook.util.annotations.LogAspect;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    Button goToUnityBtn;
    MainActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        activity = this;
        goToUnityBtn = findViewById(R.id.go_to_unity_btn);
        goToUnityBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity, UnityActivity.class);
                activity.startActivity(intent);
            }
        });

//        List<LogAspect> logAspects =  new ArrayList<>();
//        logAspects.add(LogAspect.LIFECYCLE);
//        logAspects.add(LogAspect.REST);
//        logAspects.add(LogAspect.SDK_METHODS);
//        logAspects.add(LogAspect.VIDEO_CAPTURE);
//        Smartlook.debugLoggingAspects(logAspects);
//        SmartlookUtil.startSmartlookRecording(getApplicationContext());
//        Smartlook.trackCustomEvent("App Open");
//        Smartlook.unregisterBlacklistedClass(WebView.class);
    }
}