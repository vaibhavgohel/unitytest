package com.example.unitytest;


import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;

import android.webkit.WebView;

import androidx.work.Configuration;

import com.smartlook.sdk.smartlook.Smartlook;
import com.smartlook.sdk.smartlook.util.annotations.LogAspect;

import java.util.ArrayList;
import java.util.List;


public class AppSettings extends MultiDexApplication {

    private Application.ActivityLifecycleCallbacks activityLifecycleCallbacks = new ActivityLifecycleCallbacks();
    private final String TAG = AppSettings.class.getSimpleName();


    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        Configuration config = new Configuration.Builder().build();
        this.registerActivityLifecycleCallbacks(activityLifecycleCallbacks);


        List<LogAspect> logAspects =  new ArrayList<>();
        logAspects.add(LogAspect.LIFECYCLE);
        logAspects.add(LogAspect.REST);
        logAspects.add(LogAspect.SDK_METHODS);
        logAspects.add(LogAspect.VIDEO_CAPTURE);
        Smartlook.debugLoggingAspects(logAspects);
        SmartlookUtil.startSmartlookRecording(getApplicationContext());
        Smartlook.trackCustomEvent("App Open");
        Smartlook.unregisterBlacklistedClass(WebView.class);
    }

    @Override
    public void onTerminate() {
        this.unregisterActivityLifecycleCallbacks(activityLifecycleCallbacks);
        super.onTerminate();
    }


    class ActivityLifecycleCallbacks implements Application.ActivityLifecycleCallbacks {

        @Override
        public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

        }

        @Override
        public void onActivityStarted(Activity activity) {

        }

        @Override
        public void onActivityResumed(Activity activity) {

        }

        @Override
        public void onActivityPaused(Activity activity) {

        }

        @Override
        public void onActivityStopped(Activity activity) {

        }

        @Override
        public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

        }

        @Override
        public void onActivityDestroyed(Activity activity) {
        }
    }

}

