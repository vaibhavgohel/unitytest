package com.example.unitytest;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebView;

import java.util.Date;

public class RCWebviewSoftKeyboardStateHandler implements
        SoftKeyboardStateWatcher.SoftKeyboardStateListener,
        View.OnTouchListener{
    private WebView webView;
    private ViewGroup parent;
    private long keyBoardOpenTime = 0;
    private int webViewScrollY = 0;
    private int webViewY = 0;

    public RCWebviewSoftKeyboardStateHandler(WebView webView, ViewGroup parent){
        this.webView = webView;
        this.parent = parent;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        try {
            switch (event.getAction() & MotionEvent.ACTION_MASK) {
                case MotionEvent.ACTION_DOWN:
                    //webViewX = (int) event.getRawX();
                    webViewY = (int) event.getRawY();
                    break;
                case MotionEvent.ACTION_UP:
                    if (!v.hasFocus()) {
                        v.requestFocus();
                    }
                    break;
                default:
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onSoftKeyboardOpened(int keyboardHeightInPx) {
        try {
            if (webView.getVisibility() == View.VISIBLE) {
                int height = parent.getHeight();
                int softKeyboardTop = height - keyboardHeightInPx;

                Date date = new Date();
                if (date.getTime() > keyBoardOpenTime) {
                    keyBoardOpenTime = date.getTime();
                    Log.i("keyboard", "webViewy " + webViewY + " softkeyboardTop " + softKeyboardTop);
                    if ((webViewY + 20) > softKeyboardTop) {
                        WindowManager wMgr = (WindowManager)parent.getContext().getSystemService(Context.WINDOW_SERVICE);
                        int windowHeight = wMgr.getDefaultDisplay().getHeight();

                        ViewGroup.LayoutParams vc = webView.getLayoutParams();
                        vc.height = windowHeight - keyboardHeightInPx;
                        vc.width = webView.getWidth();
                        webView.setLayoutParams(vc);

                        webViewScrollY = webViewY - softKeyboardTop;
                        //webViewScrollY = webViewY-softKeyboardTop+50;
                        Log.i("keyboard", "scroll by " + webViewScrollY + " keyboard hight " + keyboardHeightInPx +
                                " keyboard Top " + softKeyboardTop + " user y " + webViewY);
                        Log.i("keyboard", "y scroll pos before " + webView.getScrollY());
                        //webView.scrollBy(0, webViewScrollY);
                        webView.setScrollY(webView.getScrollY() + webViewScrollY);
                        Log.i("keyboard", "y scroll pos aftr " + webView.getScrollY());
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onSoftKeyboardClosed() {
        Log.i("keyboard", "keyboard is not visible");
        try {
            if (webView == null) {
                return;
            }
            if (webView.getVisibility() == View.VISIBLE) {
                webView.loadUrl("javascript:try{document.getElementById(\"add_cash_extra_div\").remove();}catch(err){}");
                webView.scrollBy(0, -webViewScrollY);
                webViewY = 0;
                webViewScrollY = 0;
                ViewGroup.LayoutParams vc = webView.getLayoutParams();
                vc.height = parent.getHeight();
                vc.width = parent.getWidth();

                webView.setLayoutParams(vc);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onSoftKeyboardOverlayChange(int keyboardHeightDiff, int keyboardHeightInPx) {
        try {
            if (webView == null) {
                return;
            }
            if (webView.getVisibility() == View.VISIBLE) {
                Date date = new Date();
                if (((date.getTime() - keyBoardOpenTime) / 100) > 3) {
                    keyBoardOpenTime = date.getTime();
                    Log.i("keyboard", "change in keyboard height " + keyboardHeightDiff + " new height" + (webView.getHeight() - keyboardHeightDiff));
                    ViewGroup.LayoutParams vc = webView.getLayoutParams();
                    vc.height = webView.getHeight() - keyboardHeightDiff;
                    vc.width = webView.getWidth();
                    webView.setLayoutParams(vc);

                    int diffScroll = 0;
                    int height = parent.getHeight();
                    int softKeyboardTop = height - keyboardHeightInPx;
                    Log.i("keyboardOverlay", "softkeyboardOverlayChange " + keyboardHeightDiff + " webViewY " + webViewY + " keyboardTop " + softKeyboardTop);
                    if (keyboardHeightDiff > 0) {
                        if (webViewY < 100) {
                            diffScroll = 0;
                        } else if ((softKeyboardTop - webViewY) < 50) {
                            diffScroll = keyboardHeightDiff + 50;
                        }
                    } else {
                        if ((softKeyboardTop - webViewY) > 100) {
                            diffScroll = keyboardHeightDiff - 50;
                        } else {
                            diffScroll = 0;
                        }
                    }
                    webView.scrollBy(0, diffScroll);
                    webViewScrollY += diffScroll;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}