package com.example.unitytest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


import java.util.Timer;
import java.util.TimerTask;


public class UnityActivity extends Activity {

    UnityActivity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unity);
        activity = this;
    }

    @Override
    protected void onResume() {
        super.onResume();

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        /* Uncomment below line to reproduce input fields in webview not getting hidden correctly and recording switching between webview and activity under it.
                           SL recording link: https://app.smartlook.com/play/shared/929320384797a2d9bd9bf26af78088ed355ff936
                           SL sdk logs can be found in logs/1.text
                        */
                        // openLoadWebView("https://www.google.com");



                        /* Uncomment below line to reproduce input fields in webview not getting hidden at all
                           SL recording link: https://app.smartlook.com/play/shared/4e2fa5e2677f7129c1d3be842f71fbfd742ee4e3
                           SL sdk logs can be found in logs/2.text
                         */
                        // openLoadWebView("https://g24x7-rcios-builds.s3.us-east-1.amazonaws.com/vaibhavtest/AllahabadPage.html?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAT6WYJSGTWASDCKU4%2F20210514%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20210514T124525Z&X-Amz-Expires=604800&X-Amz-SignedHeaders=host&X-Amz-Signature=45cf48a6af1d26ed31ed8ade136f4f8bd257acc3361999d750d89535448835fc");
                    }
                });
            }
        }, 5000);
    }

    void openLoadWebView(String url) {
        Intent intent = new Intent(activity, LoadWebView.class);
        intent.putExtra("url", url);
        intent.putExtra("orientation", "portrait");
        intent.putExtra("isBackGroundTransparent", false);
        activity.startActivity(intent);
    }
}