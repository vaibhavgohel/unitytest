package com.example.unitytest;

import android.app.ActivityManager;
import android.content.Context;
import android.util.Log;

import com.smartlook.sdk.smartlook.Smartlook;

import org.json.JSONObject;

public class SmartlookUtil {

    public static final String SL_KEY = "f3aed480098bc48dd1b05f6826d1d87bf3cbbfbb";
    public static void stopSmartlookRecording(){
        Smartlook.stopRecording();
    }

    public static void trackSmartlookEvent(String eventName, JSONObject eventProperties){
        Smartlook.trackCustomEvent(eventName,eventProperties);
    }


    public static void startSmartlookRecording(Context context){
        Smartlook.SetupOptionsBuilder builder = new Smartlook.SetupOptionsBuilder(SL_KEY)
                .useAdaptiveFramerate(false)
                .setExperimental(true);
        Smartlook.setupAndStartRecording(builder.build());
        Log.d("SmartlookUtil", getCurrentProcessName(context));
    }

    public static String getCurrentProcessName(Context context) {
        // Log.d(TAG, "getCurrentProcessName");
        int pid = android.os.Process.myPid();
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningAppProcessInfo processInfo : manager.getRunningAppProcesses())
        {
            // Log.d(TAG, processInfo.processName);
            if (processInfo.pid == pid)
                return processInfo.processName;
        }
        return "";
    }
}