package com.example.unitytest;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.util.HashMap;
import java.util.Map;

public class RCWebView extends WebView {
    private ViewGroup webViewLayout;
    String TAG = RCWebView.class.getSimpleName();
    CountDownTimer timer;

    private Map<String, String> headers=null;

    public RCWebView(Context context) {
        super(context);
        configureWebView();
    }

    public RCWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
        configureWebView();
    }

    public RCWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        configureWebView();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        webViewLayout = (ViewGroup) getParent();


        this.post(new Runnable() {
            @Override
            public void run() {
                try {
                    addSoftKeyBoardWatcher();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private void configureWebView(){
//        final String userAgent = NativeUtil.getInstance().getCustomUserAgent();
//        this.getSettings().setUserAgentString(userAgent);
        this.requestFocus(View.FOCUS_DOWN);
        this.getSettings().setDomStorageEnabled(true);
        this.getSettings().setJavaScriptEnabled(true);
        this.getSettings().setAppCacheEnabled(true);
        this.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
        this.getSettings().setAllowFileAccess(true);
        this.getSettings().setSupportZoom(true);
        this.getSettings().setBuiltInZoomControls(true);
        this.getSettings().setDisplayZoomControls(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            this.getSettings().setMediaPlaybackRequiresUserGesture(false);
        }
        this.setVerticalScrollBarEnabled(true);
        this.setHorizontalScrollBarEnabled(true);
        this.setFocusable(true);
        this.setFocusableInTouchMode(true);
    }

    public RCWebView(Activity context, View layout) {
        super(context);
        Log.i("RCWebView","constructor of RCWebView");
        configureWebView();
    }

    @Override
    public void loadUrl(String url) {
        if (url.contains("javascript:")){
            super.loadUrl(url);
        }else {
            super.loadUrl(url, headers);
        }
    }

    private void addSoftKeyBoardWatcher(){
        try {
            RCWebviewSoftKeyboardStateHandler handler
                    = new RCWebviewSoftKeyboardStateHandler(this, webViewLayout);

            setOnTouchListener(handler);
            SoftKeyboardStateWatcher softKeyboardStateWatcher = new SoftKeyboardStateWatcher(this);
            softKeyboardStateWatcher.addSoftKeyboardStateListener(handler);


        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
