package com.example.unitytest;

import android.app.Activity;
import android.os.Build;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class RCWebViewClient extends WebViewClient {

    protected Activity activity;
    protected boolean interceptReq = false;

    public RCWebViewClient(Activity activity) {
        try {
            Log.i("RCWebViewClient", "initializing RCWebViewClient");
            if (Build.VERSION.SDK_INT >= 19) {
                WebView.setWebContentsDebuggingEnabled(true);
            }
            this.activity = activity;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("RCWebViewClient", "exception in initialising RCWebViewClient", e);
        }
    }

    protected void finishWebView(){
        activity.finish();
    }

}