package com.example.unitytest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.widget.RelativeLayout;

public class LoadWebView extends Activity {

    private static final String TAG = LoadWebView.class.getSimpleName();
    protected RCWebView webview;


    //WebView Variables
    private RelativeLayout webViewLayout;
    private static ProgressDialog progressDialog;
    private String webLoadUrl;
    public static boolean downtime = false;
    protected String url;
    boolean isBackGroundTransparent = false;
    private String orientation;
    protected int count=0;
    private String removeACRHeader = "";
    private String removeMandateMobileHeader = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();

        url = intent.getStringExtra("url");
        isBackGroundTransparent = intent.getBooleanExtra("isBackGroundTransparent", false);
        orientation = intent.getStringExtra("orientation");
        try {
            if ("portrait".equals(orientation)) {
                Log.i("loadwebview", "url is add cash " + url);
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }
        } catch (Exception exception) {
        }

        webViewLayout = new RelativeLayout(this);
        webViewLayout.setBackgroundColor(Color.TRANSPARENT);

        try {
            webview = new RCWebView(this, webViewLayout) {
                @Override
                public boolean onCheckIsTextEditor() {
                    return false;
                }

            };
            Log.d("RCWebView", "" + webview);
        } catch (Exception e) {
            e.printStackTrace();
            finish();
            return;
        }


        webview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                    case MotionEvent.ACTION_UP:
                        if (!v.hasFocus()) {
                            v.requestFocus();
                        }
                        break;
                }
                return false;
            }
        });
        RelativeLayout.LayoutParams webViewParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);

//        if (NativeUtil.showNewAddCashHeader) { //REQ
//            addHeaderLayoutForAddCashJourney();
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                webViewParams.addRule(RelativeLayout.BELOW, headerLayout.getId());
//            }
//            webview.setInitialScale(1);
//            webview.getSettings().setUseWideViewPort(true);
//        }

        webview.setBackgroundColor(Color.TRANSPARENT);
        webViewLayout.setBackgroundColor(Color.TRANSPARENT);
        webViewLayout.addView(webview, webViewParams);

        // #1024 crashlytics bug fix
        progressDialog = new ProgressDialog(LoadWebView.this);
        progressDialog.setMessage("Loading ...");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        if (!this.isFinishing()) {
            // #1365 Crashlytics Fix
            try {
                progressDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        setContentView(webViewLayout);
        webview.addJavascriptInterface(new WebAppInterface(this), "Android");
        CookieSyncManager.getInstance().sync();
        webLoadUrl = url;
        Log.d(getClass().getSimpleName(), "before RCWebViewClient");
        RCWebViewClient client = new RCWebViewClient(this) {
            private boolean loadHtml = false;

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                count++;
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public WebResourceResponse shouldInterceptRequest(final WebView view, String interceptURL) {
                if (interceptURL.contains("showSecurePayPage.html")) {
                    url = interceptURL;
                }
                if (!interceptReq)
                    return super.shouldInterceptRequest(view, interceptURL);
                Log.i("interceptURL", interceptURL);
                if (removeACRHeader != null && !removeACRHeader.isEmpty()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            view.loadUrl(removeACRHeader);
                        }
                    });
                }
                if (removeMandateMobileHeader != null && !removeMandateMobileHeader.isEmpty()) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            view.loadUrl(removeMandateMobileHeader);
                        }
                    });
                }

                if (loadHtml) {
                    loadHtml = false;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                webview.loadUrl("javascript:document.addEventListener(\"DOMContentLoaded\", function(event) {\n" +
                                        "    Android.documentLoaded();\n" +
                                        "  });\n");
                            } catch (Exception exception) {
                            }
                        }
                    });
                }
                if (interceptURL.equals(webLoadUrl)) {
                    loadHtml = true;
                }
                return super.shouldInterceptRequest(view, interceptURL);
            }


            public void onPageFinished(WebView view, String interceptURL) {
                url = interceptURL;
                if (isFinishing()) {
                    return;
                }
                super.onPageFinished(view, interceptURL);

                if (!isBackGroundTransparent)
                    view.setBackgroundColor(Color.WHITE);
                try {
                    webview.loadUrl("javascript:document.addEventListener(\"DOMContentLoaded\", function(event) {\n" +
                            "    Android.documentLoaded();\n" +
                            "  });\n");
                    //** RT-7687: 0017365 after add cash failure, user getting redirected to mrc lobby bug
                    if (interceptURL.toLowerCase().contains(webLoadUrl)) {
                        view.clearHistory();
                    }

                    if (progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }
        };
        webview.setWebViewClient(client);
        webview.setWebChromeClient(new WebChromeClient() {

            @Override
            public boolean onConsoleMessage(ConsoleMessage cm) {
                Log.i("webViewConsole", String.format("%s @ line %d: %s",
                        cm.message(), cm.lineNumber(), cm.sourceId()));
                return super.onConsoleMessage(cm);
            }

            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                if(newProgress >= 100) {
                    if (progressDialog != null && progressDialog.isShowing()) {
                        try {
                            progressDialog.dismiss();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                }
            }
        });
        webview.loadUrl(webLoadUrl);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        if (inProgress)
            return true;
        return super.dispatchTouchEvent(ev);
    }

    boolean inProgress = false;

    @Override
    public void finish() {
        downtime = false;
        if (!((LoadWebView) this).isFinishing()) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }

        super.finish();
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        Log.i("VID WEBVIEW:", "BACK CLICKED");
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (downtime) {
                return true;
            }
            Log.i("VID WEBVIEW", "BAC BUTTON : can not go back");
            Log.i("WEBVIEW", "----------throwing to lobby------");
            finish();
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        Log.d("EDSCheck", "LoadWebView onDestroy true");
        if (webview != null){
            try {
                webview.destroy();
                webview = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        super.onDestroy();
    }

    public class WebAppInterface extends RCWebViewJavaScriptInterface {
        //This interface is used by javascript to call android
        Activity mContext;

        WebAppInterface(Activity c) {
            super(c, webview);
            mContext = c;
        }
    }
}