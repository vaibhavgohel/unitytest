package com.example.unitytest;

import android.app.Activity;
import android.webkit.WebView;

public class RCWebViewJavaScriptInterface {
    private static final String TAG = RCWebViewJavaScriptInterface.class.getSimpleName();

    private final Activity activity;
    private final WebView webView;

    public RCWebViewJavaScriptInterface(Activity mContext, WebView webV) {
        activity = mContext;
        webView = webV;
    }
}
